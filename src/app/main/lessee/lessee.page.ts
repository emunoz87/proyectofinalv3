import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { MainService } from '../main.service';
import { residence } from '../main.model';

@Component({
  selector: 'app-lessee',
  templateUrl: './lessee.page.html',
  styleUrls: ['./lessee.page.scss'],
})
export class LesseePage implements OnInit {
  residence: residence;
  constructor(
    private activeRouter: ActivatedRoute,
    private MainService: MainService,
    private router: Router,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.activeRouter.paramMap.subscribe(
      paramMap => {
        if(!paramMap.has('residenceId')){
          return;
        }
        const residenceId =  parseInt( paramMap.get('residenceId'));
        this.residence = this.MainService.getResidence(residenceId);
        console.log(residenceId);
      }
    );
  }

  deleteBook(){
    this.alertController.create({
      header: "Borrar Residencia",
      message: "Esta seguro que desea borrar esta residencia?",
      buttons:[
        {
          text:"No",
          role: 'no'
        },
        {
          text: 'Borrar',
          handler: () => {
            this.MainService.deleteResidence(this.residence.codeRes);
            this.router.navigate(['./lessee']);
          }
        } 
      ]
    })
    .then(
      alertEl => {
        alertEl.present();
      }
    );
    
  }

  update(code: number) {
    this.router.navigate(["/main/edit/" + code]);
  }

}
