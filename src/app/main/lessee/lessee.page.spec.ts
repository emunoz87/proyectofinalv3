import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LesseePage } from './lessee.page';

describe('LesseePage', () => {
  let component: LesseePage;
  let fixture: ComponentFixture<LesseePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LesseePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LesseePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
