import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LesseePage } from './lessee.page';

const routes: Routes = [
  {
    path: '',
    component: LesseePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LesseePageRoutingModule {}
