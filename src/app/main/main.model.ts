export interface residence{
    codeRes: number;
    nameRes: string;
    locationRes: string;
    roomsRes: number;
    bedsRes: number;
    parLot: string;
    wifi: string;
    priceDay: number;
    contactRes: string;
    contactMail: string;
}