import { Component, Input, OnInit } from '@angular/core';
import { MainService } from "./main.service";
import { residence } from "./main.model";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {
  @Input() nameRes: string;
  residence: residence[];
  
  constructor(
    private MainServices: MainService,
    private router: Router,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    console.log("Carga inicial");
    this.residence = this.MainServices.getAll();
  }

  ionViewWillEnter() {
    console.log("Se obtuvo la lista");
    this.residence = this.MainServices.getAll();
  }

  view(code: number) {
    this.router.navigate(["/main/view/" + code]);
  }

  delete(code: number) {
    this.alertController
      .create({
        header: "Borrar Residencia",
        message: "Esta seguro que desea borrar esta residencia?",
        buttons: [
          {
            text: "No",
            role: "no",
          },
          {
            text: "Borrar",
            handler: () => {
              this.MainServices.deleteResidence(code);
              this.residence = this.MainServices.getAll();
            },
          },
        ],
      })
      .then((alertEl) => {
        alertEl.present();
      });
  }

  update(code: number) {
    this.router.navigate(["/main/edit/" + code]);
  }
}
